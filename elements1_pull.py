"""
This script contains functions that pull xml data from elements
"""


import xml.etree.ElementTree as et
import requests
import creds
import os
import pickle


ABS_DIR = os.path.dirname(os.path.abspath(__file__))

# XML Namespaces
NS = {'w3': "http://www.w3.org/2005/Atom", 'api': "http://www.symplectic.co.uk/publications/api"}

CER_XML_DIR = ABS_DIR + "/xml_pulls/cer_xml_pulls/"

# API Credentials
API_USER = creds.API_USER
API_PASS = creds.API_PASS
AUTH = requests.auth.HTTPBasicAuth(API_USER, API_PASS)
BASE_URL = creds.BASE_URL


def get_profiles():
    """
    pulls profiles and saves the xml files in the xml_pulls folder. each filename contains the user's macid
    :return:
    """

    # Delete old files
    old_files = os.listdir(ABS_DIR + "/xml_pulls/user_xml_pulls")
    old_files = list(filter(lambda x: "sampleignore" not in x, old_files))  # ignore the sample xml file
    list(map(lambda x: os.remove(ABS_DIR + "/xml_pulls/user_xml_pulls/" + x), old_files))

    # pulled profiles via API
    # Notes: group id for FSS is 5, max number of profile objects per api call is 25, detail= full parameter
    url = BASE_URL + "users?groups=5&per-page=25&detail=full"

    entries = []

    while url:

        # Making the api call
        xml_data = requests.get(url=url, auth=AUTH).content
        xml_data = et.fromstring(xml_data)

        # The url for the next api calls if applicable
        url = xml_data.find(".//api:pagination/api:page[@position='next']", NS)
        if url is not None:
            url = url.attrib['href']
        else:
            url = None

        # All the XML of the individual profiles
        entries += xml_data.findall(".//w3:entry", NS)

    for profile_xml in entries:
        tree = et.ElementTree(profile_xml)
        macid = tree.find(".//api:object[@category='user']", NS).attrib['username']
        tree.write(ABS_DIR + "/xml_pulls/user_xml_pulls/" + macid + ".xml", encoding="utf-8")

        print("get_profiles() | Profile Pulled For: " + macid)


def get_cerid_macid_relationships_pickle():
    """
    gets the relationship between cerid and macid (authors). This function creates a called cerid_macid (shown below in
    code) and pickles it in the pickles folder.
    :return: None
    """

    # cerid_macid below will be saved to a pickle file to be used in other scripts and functions
    cerid_macid = dict()  # key: string (cerids), val: set (macids)
    macid_cerid = dict()  # key: string (macid), val: set (cerids)

    # List of macids
    macids = os.listdir(ABS_DIR + "/xml_pulls/user_xml_pulls")
    macids = list(filter(lambda x: "sampleignore" not in x, macids))  # ignore the sample xml file
    macids = list(map(lambda x: x.replace('.xml', ""), macids))  # ignore the sample xml file

    # Going through each macid
    for macid in macids:

        relationship_url = BASE_URL + "relationships?involving=user(username-%s)&types=8" % macid

        # In case all the relationship between the user and their publications dont fit in one api call
        while relationship_url is not None:

            relationship_request = requests.get(url=relationship_url, auth=AUTH)
            relationship_xml = et.fromstring(relationship_request.content)

            # The url for the next api calls if applicable
            next_url = relationship_xml.find(".//api:pagination/api:page[@position='next']", NS)
            if next_url is not None:
                relationship_url = next_url.attrib['href']
            else:
                relationship_url = None

            # Extracting only the cer related relationships
            entries = relationship_xml.findall(".//api:object[@category='publication'][@type='c-community-engaged-research']", NS)

            # Getting each CER publication xml
            for entry in entries:
                cer_id = entry.attrib['id']

                # updating reference variables
                macid_cerid[macid] = macid_cerid.get(macid, set())
                macid_cerid[macid].add(cer_id)

                cerid_macid[cer_id] = cerid_macid.get(cer_id, set())
                cerid_macid[cer_id].add(macid)

        print("get_cer_macid_relationships | CERS for macid " + macid + ": ",  macid_cerid.get(macid, "No CERS"))

    with open(ABS_DIR + "/pickles/cerid_macid.pickle", "wb") as handle:
        pickle.dump(cerid_macid, handle, protocol=pickle.HIGHEST_PROTOCOL)


def get_cers():
    """
    Pulls cer xml records and saves them in the xmlpulls/cer_xml_pulls folder. each filename contains cerid.
    """

    # Delete old files
    old_files = os.listdir(CER_XML_DIR)
    old_files = list(filter(lambda x: "sampleignore" not in x, old_files))  # ignore the sample xml file
    list(map(lambda x: os.remove(CER_XML_DIR + x), old_files))

    # Getting all the CER publication ids
    with open(ABS_DIR + "/pickles/cerid_macid.pickle", "rb") as handle:
        cerid_macid = pickle.load(handle)

    # Keeps track of publication we have already pulled to not hit API more than needed
    pubs_already_pulled = set()

    # Getting each CER publication xml
    for cerid in cerid_macid:

        # skipping pubs that we already have the xml for
        if cerid in pubs_already_pulled:
            continue

        pubs_already_pulled.add(cerid)

        # Making the http request
        publication_url = BASE_URL + "publications/" + cerid
        cer_xml = et.fromstring(requests.get(url=publication_url, auth=AUTH).content)

        # Creating the xml file
        tree = et.ElementTree(cer_xml)
        tree.write(CER_XML_DIR + cerid + ".xml", encoding="utf-8")

        print("get_cers() | CER XML saved for: " + cerid)


def cerid_location_pickle():
    """
    gets all the location bits from previous pulled cerxml files and then stores them in a pickled dictionary in
    the pickles folder. The dictionary is the cerid_location variable seen in the code below.

    """
    LOCATION_XPATH = ".//api:field[@name='c-cer-community-partner-organization-location']/api:addresses/"

    cer_xml_files = os.listdir(CER_XML_DIR)
    cer_xml_files = list(filter(lambda x: "sampleignore" not in x, cer_xml_files))  # ignore the sample xml file
    cer_xmls = list(map(lambda x: et.parse(CER_XML_DIR + x), cer_xml_files))
    cerids = list(map(lambda x: x.replace(".xml", ""), cer_xml_files))

    # list of list of location xmls, note some of the 2nd order lists can technically be empty
    location_xmls = list(map(lambda x: x.findall(LOCATION_XPATH, NS), cer_xmls))

    # Note the 2 lists cerids and location_xmls are of the same length
    cerid_location = {cerids[i]: location_xmls[i] for i in range(len(cerids))}

    with open(ABS_DIR + "/pickles/cerid_locationXML.pickle", "wb") as handle:
        pickle.dump(cerid_location, handle, protocol=pickle.HIGHEST_PROTOCOL)

    for cerid in cerid_location:
        print(cerid, "|", cerid_location[cerid])


# Calling all the functions
#get_profiles()
#get_cerid_macid_relationships_pickle()
#get_cers()
#cerid_location_pickle()
