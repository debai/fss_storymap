"""
This script handles the uploading of data to arcgis
"""


import requests
import json
import pandas as pd
import yaml
import hashlib
import creds
import inspect
import os

ABS_DIR = os.path.dirname(os.path.abspath(__file__))

# Arcgis Constants
TOKEN = creds.TOKEN
ARCGIS_BASE = creds.SERVICE_URL
SERVICE_URL_DELETE = ARCGIS_BASE + "0/deleteFeatures"
SERVICE_URL_ADD = ARCGIS_BASE + "0/addFeatures"
SERVICE_URL_UPDATE = ARCGIS_BASE + "0/updateFeatures"
SERVICE_URL_QUERY = ARCGIS_BASE + "0/query"
SERVICE_URL_GEOCODE = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/geocodeAddresses"
INPUT_CSV_FILE = ABS_DIR + "/csvs/formatted_combined.csv"
UPLOAD_CSV = ABS_DIR + "/csvs/arc_gis_upload.csv"


def print_seperate():
    print("\n")
    print("######################################################")
    print(inspect.stack()[1][3], "was invoked.")
    print("######################################################\n")


def print_output(output1, output2=""):
    print(inspect.stack()[1][3], " | ", output1, output2)


def get_arcgis():
    """
    Getting all objectIds in Arcgis and their hashes
    :return:  hashes_objectIds > dict()  key: row hash sha2, val: objectId of row in arc gis
    """

    hashes_objectIds = dict()  # key: row hash sha2, val: objectId of row in arc gis

    params = {'f': 'json', 'token': TOKEN, "where": "objectId is not Null", "outFields": "objectId, hash"}
    response = requests.post(url=SERVICE_URL_QUERY, params=params)
    parsed = json.loads(response.content)
    print(parsed)
    features = list(map(lambda  x: x.get("attributes", dict()), parsed["features"]))

    # filling in the hash_objectIds table
    for feature in features:
        hashes_objectIds[feature.get("hash", None)] = str(feature.get("ObjectId", None))

    if None in hashes_objectIds:
        del hashes_objectIds[None]

    print_output(hashes_objectIds)

    return hashes_objectIds


def del_arcgis():
    """

    :return: df, filled with rows that are net new and need to be uploaded
    :effect: deletes un-needed rows from arcgis AND creates a CSV file
    """
    print_seperate()

    hashes_objectIds = get_arcgis()

    # Setting up the csv file in a pandas DF
    # Import all the cer data, map the fields in elements to arcgis then make each row into a json
    df = pd.read_csv(INPUT_CSV_FILE, dtype="str")
    df.fillna("", inplace=True)

    # Importing the field map
    field_map = dict()
    with open(ABS_DIR + "/fieldMap.yml") as file:
        field_map = yaml.safe_load(file)

    # Columns in the Elements CER.csv file but NOT mentioned in the fieldMap.yml file are dropped
    drop_cols = list(filter(lambda x: x not in field_map, list(df)))
    df.drop(drop_cols, axis=1, inplace=True)

    # Columns in are renamed according to what is in the fieldMap.yml file
    df.rename(columns=field_map, inplace=True)

    # generating hash values
    df_cols = list(df)
    df_cols.sort()  # cols sorted by alphabetical order
    df = df.reindex(columns=df_cols)

    df['hash'] = df.values.sum(axis=1)
    df['hash'] = df['hash'].apply(lambda x: hashlib.sha256(x.encode()).hexdigest())

    # Comparing ARCGIS to the CSV
    # the rows where no updates need to take place are taken out of the df
    # the hash-obj id pairs that need to be deleted are left in hashes_objectIds
    for i in range(len(df)):
        if df.loc[i, "hash"] in hashes_objectIds:
            del hashes_objectIds[df.loc[i, "hash"]]
            df.drop([i], inplace=True)

    # Delete all features in ARCGIS that are not in elements anymore
    hashes_objectIds_values = list(hashes_objectIds.values())
    objectIds_del = ", ".join(hashes_objectIds_values)

    params = {'f': 'json', 'token': TOKEN, "objectIds": objectIds_del}
    request = requests.post(url=SERVICE_URL_DELETE, params=params)

    print_output("objectIds to be Deleted:", objectIds_del)
    print_output("Delete response from aricgis:", request.content)

    # Creating the csv
    df.to_csv(UPLOAD_CSV, index=False)
    print_output(UPLOAD_CSV, "has been created and will be uploaded to arcgis")


def geocode():
    """
    :return: location_xy > dict() key: str, concatenated country, state, city and street_address
                                  val: dict() key: "x" or "y", val: float representing mercator co-ordinates
    """

    print_seperate()

    # df with all the records
    df = pd.read_csv(UPLOAD_CSV, dtype="str")
    df.fillna("", inplace=True)

    # variables used to generate http requests to arcgis
    objid_count = 1
    address_records = []

    # concatenated location key is a string of: country, state, city and street_address all combined
    locations = set()  # key: str, concatenated location
    objid_location = dict()  # key; int, generated id, val: str, concatenated location key
    location_xy = dict()  # key: concatenated location key  val: dict key: 'x' or 'y', val: float, mercator co-ordinate

    for row in df.iterrows():
        row_json = row[1].to_dict()  # Making each row a json

        key = row_json.get("country", "") + row_json.get("state", "") + row_json.get("city", "") + row_json.get("street_address", "")

        if key not in locations:
            locations.add(key)
            objid_location[objid_count] = key

            address_records += [{"attributes": {
                "OBJECTID": objid_count,
                "Address": row_json.get("street_address", ""),
                "City": row_json.get("city", ""),
                "Region": row_json.get("state", ""),
                "Country": row_json.get("country", "")
            }}]

            objid_count += 1

    # splitting into calls for up to 100 addresses per api call:
    while len(address_records) > 0:
        load = []

        if len(address_records) >= 100:
            load = address_records[:100]
            address_records = address_records[100:]

        else:
            load = address_records
            address_records = []

        load = json.dumps({"records": load})
        parameters = {"addresses": load, "token": TOKEN, "f": "JSON", "outSR": 102100}
        request = requests.post(params=parameters, url=SERVICE_URL_GEOCODE)
        response = json.loads(request.content).get("locations", [])
        print_output(response)

        for item in response:

            objid = item["attributes"]["ResultID"]
            location = objid_location[objid]
            xy = item.get("location", None)

            if type(xy) != dict:
                continue

            location_xy[location] = xy

    with open(ABS_DIR + "/pickles/location_xy.json", 'w') as location_xy_file:
        json.dump(location_xy, location_xy_file)

    print_output(location_xy)


def new_cer():
    """
    :effect: upload all net new rows to arc gis
    """

    print_seperate()

    # loading the prepped csv file
    df = pd.read_csv(UPLOAD_CSV, dtype="str")
    df.fillna("", inplace=True)

    with open(ABS_DIR + "/pickles/location_xy.json", "r") as location_xy_file:
        location_xy = json.load(location_xy_file)

    for row in df.iterrows():

        row_json = row[1].to_dict()  # Making each row a json


        # the vars below attributes1 and attributes2 contains the row info which will be sent to the arcgis api
        # attributes1 contains the project summary
        # attributes2 contains all the other columns
        record1 = {"attributes": {"project_summary": row_json["project_summary"]}}
        record2 = {"attributes": dict()}


        # Filling out attributes2
        for item in row_json:
            if len(row_json[item]) > 0 and item != "project_summary":  # if column has nothing in it
                record2["attributes"][item] = row_json[item]

        # The below is for mercator co-ordinates
        key = row_json.get("country", "") + row_json.get("state", "") + row_json.get("city", "") + row_json.get(
            "street_address", "")

        if key in location_xy:
            xy = location_xy[key]
            record2["geometry"] = {"y": xy["y"], "x": xy["x"]}

        # Arc Gis API calls start here
        # the code tries to upload an item 5 times before it gives up
        object_id = -1
        count = 0
        while count < 5:
            count += 1

            # Deletes row created in previous while loop iteration that ended up in some type of error
            if object_id > -1:
                params = {'f': 'json', 'token': TOKEN, "objectIds": str(object_id)}
                response = requests.post(url=SERVICE_URL_DELETE, params=params)
                parsed0 = json.loads(response.content)
                print_output("Error Row Deletion Response for cerid " + row_json["cerid"], parsed0)

            # We upload the project_summary col here separately first because it contains the most characters
            # Can't make the row in arcgis at once with project_summary and other cols included, cause of api limit
            params = {'f': 'json', 'token': TOKEN, 'features': json.dumps([record1])}
            response = requests.post(url=SERVICE_URL_ADD, params=params)
            parsed1 = json.loads(response.content)
            print_output("Initial Col Response for cerid " + row_json["cerid"],  parsed1)

            if "error" in parsed1:
                continue

            # Uploading Additional columns
            object_id = parsed1["addResults"][0]["objectId"]
            record2["attributes"]["objectid"] = str(object_id)
            params = {'f': 'json', 'token': TOKEN, 'features': json.dumps([record2])}
            response = requests.post(url=SERVICE_URL_UPDATE, params=params)
            parsed2 = json.loads(response.content)
            print_output("Additional Cols:", parsed2)

            if "error" not in parsed2:
                print()
                break


# get_arcgis() is a helper function used in del_arcgis
#del_arcgis()
#geocode()
#new_cer()
