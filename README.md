# FSS STORY MAP Data Transfer

This repo contains code to transfer data from Elements to arcgis to make a storymap.

Notes:
- Will need to update arcgis token every now and then.

Setup:
<br>
(a) fieldMap.yml maps the columns in the csv file as a result of the 2_elements_xml_transform.py script to the columns in Arcgis
<br>
(b) creds.py should contain elements API credentials and arc gis developer token and arcgis base url

Steps:
- elements0_setup.py creates the directories needed for the etl process
<br>
- elements1_pull.py pulls User profile and Cer xml from elements. Xml is put into xml_pull directories and overwritten everytime.
<br>
- elements2_xml_transform.py ends up creating pickles that have all the necessary data in proper data structures
<br>
- elements3_format.py takes the pickles and creates csvs, notably one called formatted_combined.csv
<br>
- elements4_pushArcGis.py gets mercator coordinates using the arcgis location api
then loads the data in the csv file into the arcgis table.
<br>
- elements_runall.py runs all the scripts

