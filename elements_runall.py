

import elements0_setup
import elements1_pull
import elements2_transform
import elements3_format
import elements4_arcgis


elements0_setup.create_directories()


elements1_pull.get_profiles()
elements1_pull.get_cerid_macid_relationships_pickle()
elements1_pull.get_cers()
elements1_pull.cerid_location_pickle()


elements2_transform.cerid_cer_pickle()
elements2_transform.cerid_location_pickle()
elements2_transform.cerid_location_coresearchers_pickle()
elements2_transform.raw_combined_pickle()

elements3_format.formatted_combined_csv()
#elements3_format.raw_csvs() # isn't necessary, it just generates csvs if you want to look at what was pulled from elements

# get_arcgis() is a helper function used in del_arcgis
elements4_arcgis.del_arcgis()
elements4_arcgis.geocode()
elements4_arcgis.new_cer()

