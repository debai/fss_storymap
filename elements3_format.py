"""
This script formats and creates CSV files
"""

import pandas as pd
import pickle
import os

ABS_DIR = os.path.dirname(os.path.abspath(__file__))

# below functions format individual columns

def format_funding_sponsor(x):
    if len(x) == 0:
        return "Not funded."
    else:
        return "; ".join(x)


def format_project_website(x):
    prefix = "Want to learn more? Please visit: "

    if len(x) == 0:
        return ""
    else:
        return prefix + x


def format_researchers(x):
    names = []

    for name in x:
        one_name = name["last_name"] + ", " + name["first_name"]
        names += [one_name]

    return "; ".join(names)


def format_date(x):

    parts = x.split(" ")
    parts = list(filter(lambda x: len(x) > 0, parts))

    return "-".join(parts)


def formatted_combined_csv():
    """
    runs all the individual formatting functions and creates a csv
    """

    with open(ABS_DIR + "/pickles/raw_combined.pickle", 'rb') as handle:
        raw_combined = pickle.load(handle)

    for row in raw_combined:
        row["funding_sponsor"] = format_funding_sponsor(row["funding_sponsor"])
        row["project_website"] = format_project_website(row["project_website"])
        row["researchers"] = format_researchers(row["researchers"])
        row["community_partner_co_researchers"] = format_researchers(row["community_partner_co_researchers"])
        row["start_date"] = format_date(row["start_date"])
        row["end_date"] = format_date(row["end_date"])

    df = pd.DataFrame(raw_combined)
    df.fillna("", inplace=True)
    df.to_csv(ABS_DIR + "/csvs/formatted_combined.csv", index=False, encoding="utf-8")

    print("completed elements3_format.py")


def raw_csvs():
    """
    this is an optional function that is used to create individual csvs for cers records, locations, community
    partner coresearchers and everything combined..... MINUS the formating. This function is not run in the daily cron
    and is only here if users want to see data in separate csvs or unformatted or both
    """

    # Opening the pickled files
    with open(ABS_DIR + "/pickles/cerid_cer.pickle", "rb") as handle:
        cerid_cer = pickle.load(handle)

    with open(ABS_DIR + "/pickles/cerid_location.pickle", "rb") as handle:
        cerid_location = pickle.load(handle)

    with open(ABS_DIR + "/pickles/cerid_locationName_people.pickle", "rb") as handle:
        cerid_locationName_people = pickle.load(handle)

    with open(ABS_DIR + "/pickles/raw_combined.pickle", "rb") as handle:
        raw_combined_rows = pickle.load(handle)

    # Creating the csvs
    # CER
    rows = []
    for cerid in cerid_cer:
        rows += [vars(cerid_cer[cerid])]
    df = pd.DataFrame(rows)
    df.to_csv(ABS_DIR + "/csvs/raw_cer.csv", index=False)

    # Location
    rows = []
    for cerid in cerid_location:
        for location in cerid_location[cerid]:
            location_attributes = vars(location)
            rows += [{**{"cerid": cerid}, **location_attributes}]
    df = pd.DataFrame(rows)
    df.to_csv(ABS_DIR + "/csvs/raw_location.csv", index=False)

    # community_partner_co_researchers
    rows = []
    for cerid in cerid_locationName_people:
        for locationName in cerid_locationName_people[cerid]:
            rows += [{"cerid": cerid,
                      "location": locationName,
                      "community_partner_co_researchers": cerid_locationName_people[cerid][locationName]}]
    df = pd.DataFrame(rows)
    df.to_csv(ABS_DIR + "/csvs/raw_community_partner_co_researchers.csv", index=False)

    # Combined
    df = pd.DataFrame(raw_combined_rows)
    df.to_csv(ABS_DIR + "/csvs/raw_combined.csv", index=False)




#formatted_combined_csv()
#raw_csvs() # isn't necessary, it just generates csvs if you want to look at what was pulled from elements


