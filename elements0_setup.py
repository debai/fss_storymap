"""
ensures directories used by scripts are present
"""

import os

ABS_DIR = os.path.dirname(os.path.abspath(__file__))


def create_directories():
    """
    creates the required directories
    """

    cur_dir = os.listdir(ABS_DIR)

    if "csvs" not in cur_dir:
        os.mkdir(ABS_DIR + "/csvs")

    if "pickles" not in cur_dir:
        os.mkdir(ABS_DIR + "/pickles")

    if "xml_pulls" not in cur_dir:
        os.mkdir(ABS_DIR + "/xml_pulls")

    xml_pulls = os.listdir(ABS_DIR + "/xml_pulls")

    if "cer_xml_pulls" not in xml_pulls:
        os.mkdir(ABS_DIR + "/xml_pulls/cer_xml_pulls")

    if "user_xml_pulls" not in xml_pulls:
        os.mkdir(ABS_DIR + "/xml_pulls/user_xml_pulls")

# create_directories()
