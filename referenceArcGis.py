import requests
import json
import creds

TOKEN = creds.TOKEN
BASE_URL = creds.SERVICE_URL
"""
# add features (rows in the arcgis table)
# doc: https://developers.arcgis.com/rest/services-reference/add-features.htm
SERVICE_URL_ADD = BASE_URL + "0/addFeatures"
row = { "testfield": "hold up"}
records = json.dumps([{"attributes": row}])
params = {'f': 'json', 'token': TOKEN, 'features': records}
temp = requests.post(url=SERVICE_URL_ADD, params=params)
parsed = json.loads(temp.content)
print(parsed)


# delete feautures by objectId
# doc: https://developers.arcgis.com/rest/services-reference/add-features.htm
SERVICE_URL_DELETE = BASE_URL + "0/deleteFeatures"
params = {'f': 'json', 'token': TOKEN, "objectIds": [12, 13, 14, 15]}
temp = requests.post(url=SERVICE_URL_DELETE, params=params)
parsed = json.loads(temp.content)
print(parsed)


# delete features by where clause
SERVICE_URL_DELETE = BASE_URL + "0/deleteFeatures"

params = {'f': 'json', 'token': TOKEN, "where": "ObjectId is not Null"}
temp = requests.post(url=SERVICE_URL_DELETE, params=params)
parsed = json.loads(temp.content)
print("Deletion Results:", parsed)


SERVICE_URL_QUERY = "BASE_URL + 0/query"
params = {'f': 'json', 'token': TOKEN, "where": "objectId is not Null", "outFields": "objectId", "returnIdsOnly":"true"}
temp = requests.post(url=SERVICE_URL_QUERY, params=params)
parsed = json.loads(temp.content)
print(parsed)


##########
# Web Mercator spatial reference
URL="https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/geocodeAddresses"
addresses = {
    "records": [
        {
            "attributes": {
                "OBJECTID": 100,
                "Address": "380 New York St",
                "Neighborhood": "",
                "City": "Redlands",
                "Subregion": "",
                "Region": "CA"
            }
        }
    ]
}

# "outSR": 102100  will need to be specified in the parameters as it refers to the Web Mercator reference system
parameters = {"addresses": json.dumps(addresses), "token": TOKEN, "f": "JSON", "outSR": 102100}
request = requests.post(params=parameters, url=URL)

"""






