"""
This script takes xml files previously downloaded and pickles previously created then extracts and transforms the
data into more usable formats that are saved in the pickles folder
"""

import xml.etree.ElementTree as et
from lxml import etree
import os
import pickle

ABS_DIR = os.path.dirname(os.path.abspath(__file__))

# Directory to pull CER xml files from
CER_XML_DIR = ABS_DIR + "/xml_pulls/cer_xml_pulls/"


# XML Namespaces
NS = {'w3': "http://www.w3.org/2005/Atom", 'api': "http://www.symplectic.co.uk/publications/api"}


class Location:
    """
    Takes xml that represents single address block of a location and turns that into a class
    In Xpath terms:
    ".//api:field[@name='c-cer-community-partner-organization-location']/api:addresses/address", NS
    """
    def __init__(self, organization_xml):
        self.name = get_text(organization_xml, ".//api:line[@type='name']")
        self.institution = get_text(organization_xml, ".//api:line[@type='organisation']")
        self.street_address = get_text(organization_xml, ".//api:line[@type='street-address']")
        self.state = get_text(organization_xml, ".//api:line[@type='state']")
        self.city = get_text(organization_xml, ".//api:line[@type='city']")
        self.zip_code = get_text(organization_xml, ".//api:line[@type='zip-code']")
        self.country = get_text(organization_xml, ".//api:line[@type='country']")
        self.community_partner_organization_website = ""

        print("Location Class Created:", vars(self))

    def add_website(self, website):
        self.community_partner_organization_website = website


class Cer:
    def __init__(self, file):
        self.cer_xml = et.parse(CER_XML_DIR + file)
        self.cerid = file.replace(".xml", "")
        self.project_title = get_text(self.cer_xml, ".//api:field[@name='title'][@display-name='Project Title']/api:text")
        self.project_summary = get_text(self.cer_xml, ".//api:field[@name='abstract'][@display-name='Project Summary']/api:text")
        self.project_website = get_text(self.cer_xml, ".//api:field[@name='c-project-url']/api:text")
        self.start_date = self.get_date(".//api:field[@name='start-date']/api:date/")
        self.end_date = self.get_date(".//api:field[@name='finish-date']/api:date/")
        self.funding_sponsor = self.get_items(".//api:field[@name='c-cer-funding-sponsor']/api:items/")
        self.community_partner_organization_website = self.get_items(".//api:field[@name='c-cer-community-partner-organization-website']/api:items/")
        self.link_to_partnership_map = get_text(self.cer_xml, ".//api:field[@name='c-cer-link-to-partnership-map']/api:text")
        self.research_category = self.get_items(".//api:field[@name='c-cer-research-category']/api:keywords/")
        self.researchers = self.get_people(".//api:field[@name='authors']/api:people/")

        print("CER Class Created:", vars(self))

    def get_date(self, xpath):
        xml_date = self.cer_xml.findall(xpath, NS)  # this is a list of xml nodes each node being month/date/year
        date_pieces = dict()

        for piece in xml_date:
            tag = etree.QName(piece.tag).localname
            date_pieces[tag] = piece.text

        return date_pieces.get("year", "") + " " + date_pieces.get("month", "") + " " + date_pieces.get("day", "")

    def get_items(self, xpath):
        xml_items = self.cer_xml.findall(xpath, NS)  # this is a list of xml nodes, each being the item in question

        if len(xml_items) == 0:
            return []

        items = []

        for item in xml_items:
            items += [item.text]

        return items

    def get_people(self, xpath):
        xml_people = self.cer_xml.findall(xpath, NS)  # this is a list of xml nodes, each being the item in question

        if len(xml_people) == 0:
            return []

        people = []

        for xml_person in xml_people:

            last_name = get_text(xml_person, "./api:last-name")
            first_name = get_text(xml_person, "./api:first-names")
            people += [{"first_name": first_name, "last_name": last_name}]

        return people


def get_text(xml, search_string):
    """
    searches xml for text, handles error case where the search string cant be found
    :param xml: element tree xml
    :param search_string: what you're looking for in the xml
    :return: the value if it exists of ""
    """


    text_value = ""

    try:
        text_value = xml.find(search_string, NS).text
    except:
        pass

    return text_value


def cerid_cer_pickle():
    # Creating Cer classes
    cer_xml_files = os.listdir(CER_XML_DIR)
    cer_xml_files = list(filter(lambda x: "sampleignore" not in x, cer_xml_files))  # ignore the sample xml file
    cers = list(map(lambda x: Cer(x), cer_xml_files))

    cerid_cer = {cer.cerid: cer for cer in cers}

    with open(ABS_DIR + '/pickles/cerid_cer.pickle', 'wb') as handle:
        pickle.dump(cerid_cer, handle, protocol=pickle.HIGHEST_PROTOCOL)


def cerid_location_pickle():

    """
    saves a pickle called cerid_location_pickle.pickle which is a dict key: cerid, val: list of location classes
    """

    # Getting all the locationXMLs
    with open(ABS_DIR + '/pickles/cerid_locationXML.pickle', 'rb') as handle:
        cerid_locationXML = pickle.load(handle)

    cerid_location = dict()
    for cerid in cerid_locationXML:
        cerid_location[cerid] = list(map(lambda x: Location(x), cerid_locationXML[cerid]))

    # Location Websites
    with open(ABS_DIR + '/pickles/cerid_cer.pickle', 'rb') as handle:
        cerid_cer = pickle.load(handle)
    cers = cerid_cer.values()
    cerid_community_partner_organization_website = {cer.cerid: cer.community_partner_organization_website for cer in cers}

    # adding websites to the location classes
    for cerid in cerid_location:

        if cerid not in cerid_community_partner_organization_website:
            continue

        locations = cerid_location[cerid]
        websites = cerid_community_partner_organization_website[cerid]

        for i in range(len(locations)):
            if i + 1 <= len(websites):
                locations[i].add_website(websites[i])

    # Pickling
    with open(ABS_DIR + '/pickles/cerid_location.pickle', 'wb') as handle:
        pickle.dump(cerid_location, handle, protocol=pickle.HIGHEST_PROTOCOL)


def cerid_location_coresearchers_pickle():
    """
    cerid_locationName_people dict: key: cerid (string), val: dict: key: location_name (str), val: list of first and last names (str)
    :return:
    """

    with open(ABS_DIR + "/pickles/cerid_cer.pickle", "rb") as handle:
        cerid_cer = pickle.load(handle)

    with open(ABS_DIR + "/pickles/cerid_location.pickle", "rb") as handle:
        cerid_location = pickle.load(handle)

    cerid_locationName_people = dict()  # see function description for more info

    # initial population of cerid_locationName_people
    for cerid in cerid_location:
        for location in cerid_location[cerid]:

            old_value = cerid_locationName_people.get(cerid, dict())
            cerid_locationName_people[cerid] = {**old_value, **{location.name: list()}}

    # main loop, put all coresearchers in their respective location (organization)
    for cerid in cerid_cer:
        cer = cerid_cer[cerid].cer_xml
        xpath = ".//api:field[@name='c-cer-community-partner-co-researchers']/api:people/"
        xml_people = cer.findall(xpath, NS)  # this is a list of xml nodes, each reps a community-partner-co-researcher

        for person in xml_people:
            # org name and location name are used synomonously
            org_name = get_text(person, "./api:addresses/api:address/api:line[@type='organisation']")
            org_name = org_name.strip(" ")

            # puts the community coresearcher in the correct location (organisation)
            # note if the organization name in elements for the person doesn't match one of the location names put into
            # elements, then that name is discarded
            if org_name in cerid_locationName_people[cerid]:
                # getting first and last name of a person
                first_name = get_text(person, "./api:first-names")
                last_name = get_text(person, "./api:last-name")

                # get rid of unintentional spaces at the front and back
                first_name = first_name.strip()
                last_name = last_name.strip()

                cerid_locationName_people[cerid][org_name] += [{"last_name": last_name, "first_name": first_name}]

        # prints each cer and their locations + location partner co-researchers
        print("cerid_location_coresearchers_pickle() | CERID: " + cerid + " |", cerid_locationName_people[cerid])

    # Pickling
    with open(ABS_DIR + "/pickles/cerid_locationName_people.pickle", "wb") as handle:
        pickle.dump(cerid_locationName_people, handle, protocol=pickle.HIGHEST_PROTOCOL)


def raw_combined_pickle():
    """
    combining all the cer data such that each cerid has the following permutations:
    1) every location has a line
    2) each reasearch keyword has a line

    furthermore every for each location, the co-reasearchers from that location are added to the row

    The rows are saved as a pickle, row if dict() with key: col name, value, cell value. The pickle is called
    raw_combined.pickle and saved in the pickles folder
    """


    # Opening the pickled files
    with open(ABS_DIR + "/pickles/cerid_cer.pickle", "rb") as handle:
        cerid_cer = pickle.load(handle)

    with open(ABS_DIR + "/pickles/cerid_location.pickle", "rb") as handle:
        cerid_location = pickle.load(handle)

    with open(ABS_DIR + "/pickles/cerid_locationName_people.pickle", "rb") as handle:
        cerid_locationName_people = pickle.load(handle)

    # Creating the combined rows
    rows = []
    for cerid in cerid_cer:
        row_cer = vars(cerid_cer[cerid])
        research_categories = row_cer["research_category"]  # list of research categories need to deep copy because we get rid of the original

        del row_cer["research_category"] # this is to not conflict with the entry in the row with ONLY one category (str) as opposed to all categories (list)
        del row_cer["community_partner_organization_website"]  # this is to not conflict with the same property in Location class

        # loop to split by research categories
        for research_category in research_categories:

            # loop to split by locations
            location_list = cerid_location[cerid]  # list of location classes
            for location in location_list:
                location_name = location.name
                row_location = vars(location)

                # list of community_partner_co_researcher names for that location
                cpcr = cerid_locationName_people[cerid][location_name]

                # creating the row to be added
                rows += [{**{"community_partner_co_researchers": cpcr, "research_category": research_category},
                          **row_location,
                          **row_cer}]

    # Pickling
    with open(ABS_DIR + '/pickles/raw_combined.pickle', 'wb') as handle:
        pickle.dump(rows, handle, protocol=pickle.HIGHEST_PROTOCOL)


#cerid_cer_pickle()
#cerid_location_pickle()
#cerid_location_coresearchers_pickle()
#raw_combined_pickle()

